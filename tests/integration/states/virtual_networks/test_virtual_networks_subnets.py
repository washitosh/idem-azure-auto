import uuid

import pytest


@pytest.mark.asyncio
async def test_subnet(hub, ctx, resource_group_fixture, virtual_network_fixture):
    """
    This test provisions a subnet, describes subnets and deletes the provisioned subnet.
    """
    # Create subnet
    resource_group_name = resource_group_fixture.get("name")
    virtual_network_name = virtual_network_fixture.get("name")
    subnet_name = "idem-test-subnet-" + str(uuid.uuid4())
    subnet_parameters = {
        "properties": {
            "addressPrefix": "10.0.0.64/28",
        },
    }
    subnet_ret = await hub.states.azure.virtual_networks.subnets.present(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        parameters=subnet_parameters,
    )
    assert subnet_ret["result"], subnet_ret["comment"]
    assert not subnet_ret["changes"].get("old") and subnet_ret["changes"]["new"]

    subnet_wait_ret = await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
    hub.tool.azure.resource.check_response_payload(
        subnet_parameters, subnet_wait_ret["ret"]
    )

    # Describe subnet
    describe_ret = await hub.states.azure.virtual_networks.subnets.describe(ctx)
    assert subnet_wait_ret["ret"].get("id") in describe_ret

    # Delete subnet
    subnet_del_ret = await hub.states.azure.virtual_networks.subnets.absent(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
    )
    assert subnet_del_ret["result"], subnet_del_ret["comment"]
    assert subnet_del_ret["changes"]["old"] and not subnet_del_ret["changes"].get("new")
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
