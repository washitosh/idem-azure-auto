"""
Autogenerated using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__


"""
from collections import OrderedDict
from typing import Any
from typing import Dict


__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    resource_group_name: str,
    parameters: dict = None,
    force_update: bool = False,
) -> dict:
    r"""
    **Autogenerated function**

    Create or update Resource Groups

    Args:
        name(str): The identifier for this state.
        resource_group_name(str): The name of the resource group to create or update. Can include alphanumeric, underscore, parentheses, hyphen, period (except at end), and Unicode characters that match the allowed characters.Regex pattern: ^[-\w\._\(\)]+$.
        parameters(dict, optional): API request payload parameters. Defaults to {}.
        force_update(bool, optional): If PUT operation should be used instead of PATCH operation during resource update. Defaults to False.

    Returns:
        dict

    Examples:

        .. code-block:: sls

            resource_is_present:
              azure.resource_management.resource_groups.present:
                - name: value
                - resource_group_name: value
    """
    if parameters is None:
        parameters = {}

    subscription_id = ctx.acct.subscription_id
    response_get = await hub.exec.request.json.get(
        ctx,
        url=f"{hub.exec.azure.URL}/subscriptions/{subscription_id}/resourcegroups/{resource_group_name}?api-version=2021-04-01",
        success_codes=[200],
    )

    if force_update:
        if ctx.get("test", False):
            return dict(
                name=name,
                result=True,
                comment="Would force to update azure.resource_management.resource_groups",
            )
        response_force_put = await hub.exec.request.json.put(
            ctx,
            url=f"{hub.exec.azure.URL}/subscriptions/{subscription_id}/resourcegroups/{resource_group_name}?api-version=2021-04-01",
            success_codes=[200, 201],
            json=parameters,
        )
        if response_force_put["result"]:
            old_resource = response_get["ret"] if response_get["result"] else None
            return dict(
                name=name,
                result=True,
                old_state=old_resource,
                new_state=response_force_put["ret"],
                comment=response_force_put["comment"],
            )
        else:
            hub.log.debug(
                f"Could not force to update Resource Groups {response_force_put['comment']} {response_force_put['ret']}"
            )
            return dict(
                name=name,
                result=False,
                comment=response_force_put["comment"],
                error=response_force_put["ret"],
            )

    if not response_get["result"]:
        if ctx.get("test", False):
            return dict(
                name=name,
                result=True,
                comment="Would create azure.resource_management.resource_groups",
            )

        if response_get["status"] == 404:
            # PUT operation to create a resource
            response_put = await hub.exec.request.json.put(
                ctx,
                url=f"{hub.exec.azure.URL}/subscriptions/{subscription_id}/resourcegroups/{resource_group_name}?api-version=2021-04-01",
                success_codes=[200, 201],
                json=parameters,
            )

            if not response_put["result"]:
                hub.log.debug(
                    f"Could not create Resource Groups {response_put['comment']} {response_put['ret']}"
                )
                return dict(
                    name=name,
                    result=False,
                    comment=response_put["comment"],
                    error=response_put["ret"],
                )

            return dict(
                name=name,
                result=True,
                old_state=None,
                new_state=response_put["ret"],
                comment=response_put["comment"],
            )
        else:
            hub.log.debug(
                f"Could not get Resource Groups {response_get['comment']} {response_get['ret']}"
            )
            return dict(
                name=name,
                result=False,
                comment=response_get["comment"],
                error=response_get["ret"],
            )
    else:
        # PATCH operation to update a resource
        patch_parameters = {
            "managedBy": "managedBy",
            "name": "name",
            "properties": "properties",
            "tags": "tags",
        }
        existing_resource = response_get["ret"]
        new_parameters = hub.tool.azure.request.patch_json_content(
            patch_parameters, existing_resource, parameters
        )
        if ctx.get("test", False):
            return dict(
                name=name,
                result=True,
                comment=f"Would update azure.resource_management.resource_groups with parameters: {new_parameters}",
            )

        if not new_parameters:
            return dict(
                name=name,
                result=True,
                old_state=existing_resource,
                new_state=existing_resource,
                comment=f"'{name}' has no property need to be updated.",
            )

        response_patch = await hub.exec.request.json.patch(
            ctx,
            url=f"{hub.exec.azure.URL}/subscriptions/{subscription_id}/resourcegroups/{resource_group_name}?api-version=2021-04-01",
            success_codes=[200],
            json=new_parameters,
        )

        if not response_patch["result"]:
            hub.log.debug(
                f"Could not update Resource Groups {response_patch['comment']} {response_patch['ret']}"
            )
            return dict(
                name=name,
                result=False,
                comment=response_patch["comment"],
                error=response_patch["ret"],
            )

        return dict(
            name=name,
            result=True,
            old_state=existing_resource,
            new_state=response_patch["ret"],
            comment=response_patch["comment"],
        )


async def absent(hub, ctx, name: str, resource_group_name: str) -> dict:
    r"""
    **Autogenerated function**

    Delete Resource Groups

    Args:
        name(str): The identifier for this state.
        resource_group_name(str): The name of the resource group to delete. The name is case insensitive.Regex pattern: ^[-\w\._\(\)]+$.

    Returns:
        dict

    Examples:

        .. code-block:: sls

            resource_is_absent:
              azure.resource_management.resource_groups.absent:
                - name: value
                - resource_group_name: value
    """

    subscription_id = ctx.acct.subscription_id
    response_get = await hub.exec.request.json.get(
        ctx,
        url=f"{hub.exec.azure.URL}/subscriptions/{subscription_id}/resourcegroups/{resource_group_name}?api-version=2021-04-01",
        success_codes=[200],
    )
    if response_get["result"]:
        if ctx.get("test", False):
            return dict(
                name=name,
                result=True,
                comment="Would delete azure.resource_management.resource_groups",
            )

        existing_resource = response_get["ret"]
        response_delete = await hub.exec.request.raw.delete(
            ctx,
            url=f"{hub.exec.azure.URL}/subscriptions/{subscription_id}/resourcegroups/{resource_group_name}?api-version=2021-04-01",
            success_codes=[200, 202],
        )

        if not response_delete["result"]:
            hub.log.debug(
                f"Could not delete Resource Groups {response_delete['comment']} {response_delete['ret']}"
            )
            return dict(
                name=name,
                result=False,
                comment=response_delete["comment"],
                error=response_delete["ret"],
            )

        return dict(
            name=name,
            result=True,
            old_state=existing_resource,
            new_state={},
            comment=response_delete["comment"],
        )
    elif response_get["status"] == 404:
        # If Azure returns 'Not Found' error, it means the resource has been absent.
        return dict(
            name=name,
            result=True,
            old_state=None,
            new_state=None,
            comment=f"'{name}' already absent",
        )
    else:
        hub.log.debug(
            f"Could not get Resource Groups {response_get['comment']} {response_get['ret']}"
        )
        return dict(
            name=name,
            result=False,
            comment=response_get["comment"],
            error=response_get["ret"],
        )


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    r"""
    **Autogenerated function**

    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    List all Resource Groups under the same subscription


    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: bash

            $ idem describe azure.resource_management.resource_groups
    """

    result = {}
    subscription_id = ctx.acct.subscription_id
    uri_parameters = OrderedDict({"resourceGroups": "resource_group_name"})
    async for page_result in hub.tool.azure.request.paginate(
        ctx,
        url=f"{hub.exec.azure.URL}/subscriptions/{subscription_id}/resourcegroups?api-version=2021-04-01",
        success_codes=[200],
    ):
        resource_list = page_result.get("value", None)
        if resource_list:
            for resource in resource_list:
                uri_parameter_values = hub.tool.azure.uri.get_parameter_value(
                    resource["id"], uri_parameters
                )
                result[resource["id"]] = {
                    f"azure.resource_management.resource_groups.present": uri_parameter_values
                    + [{"parameters": resource}]
                }
    return result
